var gulp = require('gulp');
var git = require('gulp-git');
var express = require('express');
var bodyParser = require('body-parser');
var shell = require('gulp-shell');

// CLONE
gulp.task('clone', ['clone git', 'build production clone'])
gulp.task('clone git', function(){
	return gulp.src('')
	    .pipe(shell([
	    	'rm -rf ~/jumper/servers/production/node_jumper',
			'sudo -u bermangrp git -C ~/jumper/servers/production clone git@gitlab.com:bermangrp/node_jumper.git',
			'echo done clone'
	    ]))
});
gulp.task('build production clone', ['clone git'], function(){
	buildProduction();
})


// PULL
gulp.task('pull', ['pull git', 'build production pull'])
gulp.task('pull git', function(){
	console.log('pull git')
	return gulp.src('')
	    .pipe(shell([
			'sudo -u bermangrp git -C ~/jumper/servers/production/node_jumper pull git@gitlab.com:bermangrp/node_jumper.git',
			'echo done pull'
	    ]))
})
gulp.task('build production pull', ['pull git'], function(){
	buildProduction();
})


// BUILD
function buildProduction(){
	gulp.start('build');
}

gulp.task('build', function(){
	return gulp.src('')
	    .pipe(shell([
	    	'whoami',
	    	// '(cd /home/bermangrp/jumper/servers/production/node_jumper/servers/development/ && sudo -u bermangrp npm install)',
	    	// 'sudo forever start ~/jumper/servers/production/node_jumper/servers/development/koa.js'
	    ]))
})


// START SERVER
gulp.task('default', ['start server']);
gulp.task('start server', function(){
	var app = express();

	app.use( bodyParser.json() );
	app.use(bodyParser.urlencoded({
		extended: true
	})); 

	app.post('/', function (req, res) {
		res.send('end');
		gulp.start('pull');
	});

	var server = app.listen(5000, function () {
		var host = server.address().address;
		var port = server.address().port;
		console.log('listening for posts')
	});

	gulp.start('clone');
});

gulp.start('default');

