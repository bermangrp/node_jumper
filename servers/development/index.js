var devMode = true;
var gulp = require('gulp');
var bower = require('gulp-bower');
var rename = require("gulp-rename");
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var nodemon = require('gulp-nodemon');
var less = require('gulp-less');
var minifyCSS = require('gulp-minify-css');
var watchLess = require('gulp-watch-less');
var install = require("gulp-install");
var watch = require('gulp-watch');
var git = require('gulp-git');
var shell = require('gulp-shell')

gulp.task('bower', function() {
	return bower('./bower_components');
});

gulp.task('build-js', ['bower'], function() {
	fileArray = (devMode) ? [
		'./bower_components/**/*.min.js',
		'./bower_components/**/*-min.js',
		'./bower_components/**/backbone.js',
		'./machines/main.js'
	] : [
		'./bower_components/**/jquery.min.js',
		'./bower_components/**/underscore-min.js',
		'./bower_components/**/backbone-min.js'
	];

	return gulp.src(fileArray).pipe(sourcemaps.init())
		.pipe(uglify())
		.pipe(concat('all.min.js'))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('public/js'));
});

gulp.task('build-less', ['build-js'], function(){
    gulp.src('./styles/main.less')
        .pipe(less())
		.pipe(minifyCSS())
		.pipe(rename({
			extname: '.min.css'
		}))
		.pipe(gulp.dest('./public/css'));
})

gulp.task('deploy-production', ['bower', 'build-js', 'build-less'], function () {
	return gulp.src('')
	    .pipe(shell([
			'sudo forever stopall',
			'sudo forever start koa.js'
	    ]))
})

var productionTasks = [
	'bower',
	'build-js',
	'build-less',
	'deploy-production'
]

gulp.task('default', productionTasks);

gulp.start();


// gulp.task('nodemon', ['build-less'], function () {
// 	nodemon({
// 		script: 'koa.js',
// 		ext: 'js html',
// 		ignore: ['./public/'],
// 		env: { 'NODE_ENV': 'development' }
// 	})
// })

// gulp.task('watch-less', ['deploy-production'], function(){
// 	watchLess('./styles/main.less', function(){
// 		 gulp.start('build-less', function(){
// 		 	console.log('css done build')
// 		 });
// 	})
// })

// gulp.task('watch-js', ['deploy-production'], function(){
// 	watch('./machines/main.js', function(){
// 		 gulp.start('build-js', function(){
// 		 	console.log('js done build')
// 		 });
// 	})
// })

// gulp.task('gulp-install', function(){
// 	gulp.src(['./package.json'])
// 	  .pipe(install());
// })

// gulp.task('clone', function(){
// 	git.clone('https://gitlab.com/wfaye/the-jump.git', function (err) {
// 		if (err) throw err;
// 	});
// });

var developmentTasks = [
	'bower',
	'build-js',
	'build-less',
	'watch-less',
	'watch-js',
	'deploy-production'
];