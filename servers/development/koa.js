var koa = require('koa');
var fs = require('fs');
var app = module.exports = koa();
var path = require('path');
var extname = path.extname;
var publicPath = __dirname + '/public';

app.use(function *() {
	var pathRequest = this.path;
	var path = publicPath + pathRequest;
	var isHomePage = publicPath + '/' == path;

	if(pathRequest != '/favicon.ico'){
		if(isHomePage){
			path = path + 'index.html';
		}

		var fstat = yield stat(path);

		if (fstat.isFile()) {
			this.type = extname(path);
			this.body = fs.createReadStream(path);
		}
	}
});

if(!module.parent){
	app.listen(80)
}

function stat(file) {
 	return function (done) {
 		fs.stat(file, done);
 	};
}

var appTest = koa();

appTest.use(function *(){
	this.body = 'Hello World';
});

appTest.listen(3000);