var express = require('express');
var app = express();

app.use(express.static('public'));

import * as test from "./public/test.js"

var server = app.listen(3000, function () {
	var host = server.address().address;
	var port = server.address().port;

	console.log('Example app listening at http://%s:%s', host, port);
	console.log(test.count_even_numbers(10))
});