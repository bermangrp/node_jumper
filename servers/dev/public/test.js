// http://es6-features.org
	// Constants
	// Scoping
// Arrow Functions
// Extended Parameter Handling
// Template Strings
// Extended Literals
// Enhanced Regular Expression
// Enhanced Object Properties
// Destructuring Assignment
// Modules
// Classes
// Symbol Type
// Iterators
// Generators
// Map/Set & WeakMap/WeakSet
// Typed Arrays
// New Built-In Methods
// Promises
// Meta-Programming

const returnObject = {}

export function count_even_numbers(max_even_number){
	var evens = [];
	var odds;
	var pairs;
	var nums;

	for (let i = 0; i <= max_even_number; i++) {
		{
			function is_even(i) { return i%2 == 0; };
			{
				function is_even(i) { return i%2 == 0 && i != 0; };
			};
			if(is_even(i)){
				evens.push(i);
			};
		};
	};

	odds = evens.map(v => v + 1);
	pairs = evens.map(v => ({
		even: v,
		odd: v + 1
	}));
	nums = evens.map((v, i) => v + i);

	return evens;
};