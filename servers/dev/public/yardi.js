const company_code = 'c00000077476';
const property_codes = ['p0236238','p0236239','p0236240','p0290053','p0295648','p0295650','p0296671','p0325210','p0325967','p0332802','p0332803','p0332807','p0332896','p0335077','p0353270','p0361416','p0362629','p0362633','p0362639','p0362640','p0362641','p0373740'];

class PropertyItem {

	constructor (options){

		this.property_code = options.property_code;
		this.company_code = company_code;

	}

	get property_info (){

		console.log(this.property_code, this.company_code)
		this.property_request();
	}

	property_request (){
		
		let xhr = new window.XMLHttpRequest;
		xhr.onreadystatechange=function(){
			if (xhr.readyState==4 && xhr.status==200){
				console.log(xhr.responseText);
			}
		}
		xhr.open("GET", "https://www.rentcafe.com/rentcafeapi.aspx?requestType=property&type=marketingData&companyCode=" + this.company_code + "&propertyCode=" + this.property_code, true);
		xhr.send();

	}

}

function initialize_application(){
	let oncer = 0;
	property_codes.forEach(this_property_code => {

		if(oncer == 0){

			let thisPropertyItem = new PropertyItem({
				property_code: this_property_code
			});

			thisPropertyItem.property_info;

			oncer++;
		}

	});

}

initialize_application();