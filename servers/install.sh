# # give user no password access
# 	echo 'echo "'$1' ALL = NOPASSWD:ALL" >> /etc/sudoers.d/00_p' | sudo -s;

# install mongo
	sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10;
	echo "deb http://repo.mongodb.org/apt/ubuntu "$(lsb_release -sc)"/mongodb-org/3.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.0.list;
	sudo apt-get update;
	sudo apt-get install -y mongodb-org;
	sudo service mongod start;

# install git
	sudo apt-get install -y git;

# install nodemon
	sudo npm install -g nodemon;

# install gulp
	sudo npm install -g gulp;

# install n and use latest
	sudo npm install -g n;
	sudo n io latest;

# install forever
	sudo npm install forever -g;

# generate ssh key for gitlab and add to account
	ssh-keygen -t rsa -C "wfaye@bermangrp.com" -N "" -f ~/.ssh/id_rsa;
	GITLAB_KEY_VAR=`cat ~/.ssh/id_rsa.pub`;
	curl -X POST "https://gitlab.com/api/v3/user/keys" -H 'PRIVATE-TOKEN:'$3 -H "Content-Type: application/json" -d'{"title":"'$2'-server-key","key":"'"$GITLAB_KEY_VAR"'"}';

# create new gitlab project
	curl --header 'PRIVATE-TOKEN:'$3 https://gitlab.com/api/v3/projects --data 'name=node_'$2'&namespace_id=126061';
	THIS_SERVER_IP=`ifconfig eth0 | grep "inet addr" | awk -F: '{print $2}' | awk '{print $1}'`;
	curl --header 'PRIVATE-TOKEN:'$3 https://gitlab.com/api/v3/projects/bermangrp%2Fnode_$2/hooks --data 'url=http://'$THIS_SERVER_IP':5000/';

# install helper server npm
	cd /home/$1/$2/servers/helper;
	sudo npm install;

# # remove no password access 1Berman2008
# 	sudo rm /etc/sudoers.d/00_p;

# start helper server
	sudo forever start index.js;