# sample: sh the-jump.sh -i -p jumper -d 104.236.137.64 -l bermangrp;
# sample: sh the-jump.sh -i -p PROJECT_NAME -d DOMAIN_OR_URL -l LOGIN_NAME
# set acceptable paramenters for script
args=`getopt iupdl: $*`;

# check the error code of the last executed statement using $? (errors only happens if parameters other than those set above are called)
if [ $? != 0 ]
then
    # a bad argument was found, notify user with Special Exit Code 2 - Misuse of shell builtins
    echo 'Proper Usage:';
    echo '';
    echo '  Mandatory arguments:';
    echo '    -p PROJECT_NAME';
    echo '    -d DOMAIN_OR_URL';
    echo '    -l LOGIN_NAME ';
    echo '';
    echo '  Only one of these arguments may be set:';
    echo '    -i will trigger install mode';
    echo '    -u will trigger update mode';
    echo '';
    exit 2;
else
    # arguments are ok, processing script
    echo 'processing';
fi

# determine mode
set -- $args;

project_name=0;
domain_url=0;
login_name=0;

for i in $args; do
    case $i in
        -i)
            install=1;
            update=0;
            shift;
        ;;

        -u)
            update=1;
            install=0;
            shift;
        ;;

        -p)
            project_name=$3;
            shift; shift;
        ;;

        -d)
            domain_url=$3;
            shift; shift;
        ;;

        -l)
            login_name=$3;
            shift; shift;
        ;;

    esac
done

if [ $project_name != 0 ] && [ $domain_url != 0 ] && [ $login_name != 0 ]
then
    if [ $install != 0 ]
    then
        # UNINSTALL FOR TESTING
            ssh $login_name@$domain_url -t 'rm -rf .ssh '$project_name'/';
        # UNINSTALL FOR TESTING


        # update project name variables in project files with project name argument
            sed -i '' -e 's/PROJECT_NAME/'$project_name'/g' servers/helper/index.js;

        # upload local public keys
        # create directories
        # run installation shell
            echo "uploading";
            echo "installing";

        # exchange ssh keys with local and server
        # create project folders
            cat ~/.ssh/id_rsa.pub | ssh $login_name@$domain_url 'mkdir ~/.ssh; chmod 0700 ~/.ssh; cat >> ~/.ssh/authorized_keys; mkdir ~/'$project_name'/; mkdir ~/'$project_name'/servers/;';

        # upload servers
            scp -r servers $login_name@$domain_url:~/$project_name;

        # run server installation script
            ssh -t $login_name@$domain_url 'sh ~/'$project_name'/servers/install.sh '$login_name $project_name $GITLAB_TOKEN';';

        # push this project up to git
            echo "done server setup1";
            rm -rf .git;
            git init;
            git add -A;
            git commit -m "first commit";
            git remote add origin git@gitlab.com:bermangrp/node_$project_name.git;
            git push -u origin master;

    fi

    if [ $update != 0 ]
    then
        echo "update";
    fi
else
    echo "not enough info to run";
    exit 2;
fi

    echo 'done';